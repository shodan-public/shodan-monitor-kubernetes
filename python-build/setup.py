"""
Setup to install kube-shodan as a Python package.
"""

from datetime import datetime
from os import getenv
from setuptools import find_packages, setup

setup(
    name='shodan-monitor-k8s',
    version=getenv('GITHUB_REF',
                   default=datetime.now().strftime('%Y.%m.%d.dev%H%M%S')).lstrip('refs/tags/'),
    description='shodan-monitor-k8s registers all public IPs of a Kubernetes '
                'cluster to monitor.shodan.io.',
    url='https://gitlab.com/shodan-public/shodan-monitor-k8srnetes',
    license='GPL',
    classifiers=[
        'Programming Language :: Python :: 3'
    ],
    python_requires='>=3.7',
    install_requires=['click', 'kubernetes', 'shodan'],
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'shodan-monitor-k8s=kubeshodan.main:main'
        ]
    }
)
