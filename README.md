# shodan-monitor-kubernetes

shodan-monitor-kubernets get all the public IPs of Kubernetes cluster then add to [Shodan Monitor](https://monitor.shodan.io). If IPs that are removed from the cluster, it is also removed on Shodan Monitor. So Shodan will only monitor public IPs that kubernetes hold.

## Installation

### Docker Cointainer

python-build repo contains Python code to run the process in kubernetes. You can build Docker images from python-build repo then push to some Docker registry that you want.

```
docker build -t registry.gitlab.com/shodan-public/shodan-monitor-kubernetes:1.0.0 ./python-build                  
docker push registry.gitlab.com/shodan-public/shodan-monitor-kubernetes:1.0.0    
```                     

### Helm 
Using `kubectl` to connect to your kubernets. Note that you should have **kubeconfig.yaml** file. Also a *SHODAN_API_KEY* from your [Shodan Account](https://account.shodan.io).

```
export KUBECONFIG=/path/to/kubeconfig.yaml 

# check connect
kubectl get nodes

# add shodan-monitor-kubernetes helm repo
helm repo add shodan-monitor-kubernetes "https://shodan-public.gitlab.io/shodan-monitor-kubernetes"
helm repo update

# install shodan-monitor-k8s
helm install --set shodanApiToken=SHODAN_API_KEY  shodan-monitor-k8s shodan-monitor-kubernetes/shodan-monitor-k8s
```
## Credit
This application uses Open Source components. You can find the source code of their open source projects below. We acknowledge and are grateful to the developer for his contributions.

Project: kube-shodan https://github.com/ekeih/kube-shodan
